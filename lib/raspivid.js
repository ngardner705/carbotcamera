"use strict";

const util      = require('util');
const spawn     = require('child_process').spawn;
const merge     = require('mout/object/merge');

const Server    = require('./_server');


class RpiServer extends Server {

  constructor(server, opts) {
    super(server, merge({
      fps : 20,
    }, opts));
  }

  start_camera() {
    console.log('starting camera');
    this.streamer = spawn('raspivid', ['-t', '0','-v','-o', '-', '-w', this.options.width, '-h', this.options.height, '-fps', this.options.fps, '-pf', 'baseline']);
  }

  get_feed() {
    this.start_camera();
    return this.streamer.stdout;
  }

  stop_feed() {
    console.log('stopping feed');
    this.streamer.stdin.pause();
    this.streamer.kill();
  }

  /*get_feed() {
    var msk = "raspivid -t 0 -v -o - -w %d -h %d -fps %d";
    var cmd = util.format(msk, this.options.width, this.options.height, this.options.fps);
    console.log(cmd);
    var streamer = spawn('raspivid', ['-t', '0','-v','-o', '-', '-w', this.options.width, '-h', this.options.height, '-fps', this.options.fps, '-pf', 'baseline']);
    streamer.on("exit", function(code){
      console.log("Failure", code);
    });

    return streamer.stdout;
  }*/

};



module.exports = RpiServer;
